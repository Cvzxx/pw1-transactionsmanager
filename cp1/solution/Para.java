package cp1.solution;

import cp1.base.ResourceId;
import cp1.base.ResourceOperation;

public class Para {
    private ResourceId rid;
    private ResourceOperation op;

    public Para(ResourceId rid, ResourceOperation op) {
        this.rid = rid;
        this.op = op;
    }

    public ResourceId getRid() {
        return rid;
    }

    public void setRid(ResourceId rid) {
        this.rid = rid;
    }

    public ResourceOperation getOp() {
        return op;
    }

    public void setOp(ResourceOperation op) {
        this.op = op;
    }
}
