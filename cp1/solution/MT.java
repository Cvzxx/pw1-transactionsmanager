package cp1.solution;

import cp1.base.*;

import java.util.Collection;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

public class MT implements TransactionManager {
    private ConcurrentHashMap<ResourceId, Resource> originalResources; //mapowanie zasob id na zasob
    private LocalTimeProvider timeQueries;

    private ConcurrentHashMap<ResourceId, Semaphore> resourceId; //kazdy zasob to jeden semafor
    private ConcurrentHashMap<ResourceId, Long> resourceThread; // jaki watek(ID) zajal zasob
    private ConcurrentHashMap<Long, Stack<Para>> mappedOperations; // watek i operacje jakie wykonal
    private ConcurrentHashMap<Long, Long> timeStarted; // watek i czas zaczecia transakcji
    private ConcurrentHashMap<Long, ResourceId> waiting; // watek i na jakim zasobie wisi(czeka) - sciezka w grafia
    private ConcurrentHashMap<Long, Thread> threadsInTM; // aktywane watki w MT
    private ConcurrentHashMap<Long, Thread> abortedThreads; // anulowane watki w MT
    private Semaphore mutex;

    public MT(Collection<Resource> originalResources, LocalTimeProvider timeQueries) {
        this.originalResources = new ConcurrentHashMap<>();

        originalResources.forEach(temp -> this.originalResources.put(temp.getId(), temp));

        resourceId = new ConcurrentHashMap<>();
        originalResources.forEach(temp -> resourceId.put(temp.getId(), new Semaphore(1, true)));


        mutex = new Semaphore(1, true);
        this.timeQueries = timeQueries;
        threadsInTM = new ConcurrentHashMap<>();
        abortedThreads = new ConcurrentHashMap<>();
        mappedOperations = new ConcurrentHashMap<>();
        resourceThread = new ConcurrentHashMap<>();
        timeStarted = new ConcurrentHashMap<>();
        waiting = new ConcurrentHashMap<>();
    }

    @Override
    public void startTransaction() throws AnotherTransactionActiveException {
        if (isTransactionActive()) {
            throw new cp1.base.AnotherTransactionActiveException();
        } else {
            timeStarted.put(Thread.currentThread().getId(), timeQueries.getTime());
            threadsInTM.put(Thread.currentThread().getId(), Thread.currentThread());
            Stack<Para> stack = new Stack<>();
            mappedOperations.put(Thread.currentThread().getId(), stack);
        }
    }

    @Override
    public void operateOnResourceInCurrentTransaction(ResourceId rid, ResourceOperation operation) throws NoActiveTransactionException, UnknownResourceIdException, ActiveTransactionAborted, ResourceOperationException, InterruptedException {

        if (!isTransactionActive()) {
            throw new cp1.base.NoActiveTransactionException();
        }

        if (!resourceId.containsKey(rid)) {
            throw new UnknownResourceIdException(rid);
        }

        if (isTransactionAborted()) {
            throw new ActiveTransactionAborted();
        }


        if (resourceThread.containsKey(rid) && resourceThread.get(rid) == Thread.currentThread().getId()) {
            operation.execute(originalResources.get(rid));
            mappedOperations.get(Thread.currentThread().getId()).push(new Para(rid, operation));

            if (Thread.currentThread().isInterrupted()) {
                mappedOperations.get(Thread.currentThread().getId()).pop();
                operation.undo(originalResources.get(rid));
                if (abortedThreads.containsKey(Thread.currentThread().getId()))
                    throw new ActiveTransactionAborted();
                else
                    throw new InterruptedException();
            }

        } else {

            try {
                mutex.acquire();
                waiting.put(Thread.currentThread().getId(), rid);
                if (resourceThread.containsKey(rid)) {
                    checkCycle(rid);
                }
                mutex.release();

                if (Thread.currentThread().isInterrupted()) {
                    waiting.remove(Thread.currentThread().getId());
                    if (abortedThreads.containsKey(Thread.currentThread().getId()))
                        throw new ActiveTransactionAborted();
                    else
                        throw new InterruptedException();
                }

                resourceId.get(rid).acquire();
            } catch (InterruptedException e) {
                waiting.remove(Thread.currentThread().getId());

                if (abortedThreads.containsKey(Thread.currentThread().getId()))
                    throw new ActiveTransactionAborted();
                else
                    throw new InterruptedException();

            }


            resourceThread.put(rid, Thread.currentThread().getId());
            waiting.remove(Thread.currentThread().getId());
            operation.execute(originalResources.get(rid));
            mappedOperations.get(Thread.currentThread().getId()).push(new Para(rid, operation));

            if (Thread.currentThread().isInterrupted()) {
                mappedOperations.get(Thread.currentThread().getId()).pop();
                operation.undo(originalResources.get(rid));
                if (abortedThreads.containsKey(Thread.currentThread().getId()))
                    throw new ActiveTransactionAborted();
                else
                    throw new InterruptedException();
            }
        }


    }

    @Override
    public void commitCurrentTransaction() throws NoActiveTransactionException, ActiveTransactionAborted {
        if (!isTransactionActive()) {
            throw new cp1.base.NoActiveTransactionException();
        }

        if (isTransactionAborted()) {
            throw new ActiveTransactionAborted();
        }

        mutex.acquireUninterruptibly();
        clear();

        mutex.release();
    }

    @Override
    public void rollbackCurrentTransaction() {

        mutex.acquireUninterruptibly();
        if (isTransactionActive()) {
            while (!mappedOperations.get(Thread.currentThread().getId()).empty()) {
                Para para = mappedOperations.get(Thread.currentThread().getId()).pop();
                para.getOp().undo(originalResources.get(para.getRid()));
            }

            clear();
        }
        mutex.release();
    }

    @Override
    public boolean isTransactionActive() {
        return threadsInTM.containsKey(Thread.currentThread().getId());
    }

    @Override
    public boolean isTransactionAborted() {
        return abortedThreads.containsKey(Thread.currentThread().getId());
    }

    //zwalnianie zasobow, czyszczenie kolekcji
    private void clear() {
        mappedOperations.remove(Thread.currentThread().getId());

        waiting.remove(Thread.currentThread().getId()); //usuwanie krawedzi, czekanie na zasob
        threadsInTM.remove(Thread.currentThread().getId()); //koncze transakcje (usuwam)
        abortedThreads.remove(Thread.currentThread().getId());
        timeStarted.remove(Thread.currentThread().getId());

        resourceThread.forEach((k, v) -> {
            if (v == Thread.currentThread().getId()) {
                resourceId.get(k).release(); //zwalniamy blokowane zasoby
            }
        });

        resourceThread.entrySet().removeIf(e -> (e.getValue() == Thread.currentThread().getId())); //usywamy blokowane zasoby
    }

    //szukanie cyklu i ewentulane usuwanie watka(ustawia flage aborted i usuwa krawedz z grafu)
    private void checkCycle(ResourceId rid) {

        long maxId = Thread.currentThread().getId();
        long maxTime = timeStarted.get(maxId);

        long currId = resourceThread.get(rid);
        long currTime = timeStarted.get(currId);

        if ((currTime > maxTime) || (currTime == maxTime && currId > maxId)) {
            maxId = currId;
            maxTime = currTime;
        }

        while (waiting.containsKey(currId)) {

            ResourceId res = waiting.get(currId);
            currId = resourceThread.get(res);
            currTime = timeStarted.get(currId);


            if ((currTime > maxTime) || (currTime == maxTime && currId > maxId)) {
                maxId = currId;
                maxTime = currTime;
            }


            if (Thread.currentThread().getId() == currId) {
                abortedThreads.put(maxId, threadsInTM.get(maxId));
                waiting.remove(maxId);
                threadsInTM.get(maxId).interrupt();
                break;
            }
        }
    }
}

